import React, { Component } from 'react';
import './App.css';
import Events from './components/Events';
import AddEvent from './components/AddEvent';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import EventDetails from './components/EventDetails';
import EditEvent from './components/EditEvent';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events: []
    }
  }

  //Chargement des données de la base au montage du composant
  componentDidMount() {
    fetch('http://localhost:8081/events')
      .then((response) => response.json())
      .then((result) => this.setState({ events: result.rows }));
  }


  //Utilisation du routage pour naviguer entre les composants dans l'application
  render() {
    return (
      <Router>
        <div className="App">
          <Route exact path="/addEvent" component={AddEvent} />
          <Route exact path="/" component={() => <Events events={this.state.events} />} />
          <Route exact path="/editEvent/:id" component={EditEvent} />
          <Route path="/details/:id" component={EventDetails} />
        </div>
      </Router>
    );
  }
  
}


export default App;



