import React, { Component } from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'

class EventItem extends Component {
    render() {
        return (
            <li className="list-group-item">
            <span>{this.props.event.nom} à {this.props.event.lieu} - {moment(this.props.event.dateevent).format('DD/MM/YYYY')} à {this.props.event.timeevent}</span>
             <button id="btn-delete" onClick={() => this.deleteItem(this.props.event.id)} className="btn btn-danger btn-xs pull-right remove-item">
            <span>Supprimer</span>
              </button>
              <Link to={{pathname:"/editEvent/"+this.props.event.id, event:this.props.event}}> 
                <button id="btn-modify" className="btn btn-warning btn-xs pull-right modify-item">
                  <span>Modifier</span>
                </button>
              </Link>
            </li> 
        )
    }

    //Fonction de suppression d'un évènement par son ID
    deleteItem = (i) => {
        /* let events = this.state.events;
        events.splice(i,1);
        this.setState({
          events: events
        }) */
        if(window.confirm('Voulez vous vraiment supprimer cet évènement ?')){
          fetch('http://localhost:8081/events/'+i, {
            method: 'DELETE',
            headers: {
              'Accept' : 'application/json',
              'Content-Type': 'application/json',
            }, 
          })
          window.location.reload();
        }
      }
}

export default EventItem;

