import React, { Component } from 'react'
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
import fr from 'date-fns/locale/fr';
import { registerLocale } from  "react-datepicker";
import TimePicker from 'react-time-picker'
import moment from 'moment'
import { Link } from 'react-router-dom'
registerLocale('fr', fr)


export default class AddEvent extends Component {

    //Constructeur
    constructor(props){
        super(props);
        this.state = {
        name: '',
        lieu: '',
        dateevent: new Date(),
        timeevent: moment(new Date()).format("HH:mm"),
        noData: false,
        events: [],
        redirect: null
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    //Handler pour changement de nom dans l'input
      OnChangeName(event) {
        this.setState({
          name: event.target.value,
        });
      }
    
      //Handler pour changement de lieu dans l'input
      OnChangeLieu(event) {
        this.setState({
          lieu: event.target.value,
        });
      }
    
      //Handler pour changement de date dans l'input
      onChangeDate = date => this.setState({dateevent : date})
    
      //Handler pour changement de l'heure dans l'input
      onChangeTime = val => this.setState({timeevent : val})
      
      //Fonction d'ajout d'un évènement dans la base 
      handleSubmit(event) {
        event.preventDefault();
        fetch('http://localhost:8081/events', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            nom: this.state.name,
            lieu: this.state.lieu,
            dateevent: this.state.dateevent,
            timeevent: this.state.timeevent 
          }),
        }).then((response) => console.log(response));
        this.setState({
          lieu: '',
        });
        this.setState({
          name: '',
        });
        alert("L'évènement a bien été ajouté !")
        window.location.reload();
      }

    render() {
      console.log(moment(this.state.dateevent).format("/MM/YYYY"))
        return (
            <div>
                <h1>Ajouter un évènement</h1>
                <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Nom de l'évènement</label>
            <input value={this.state.name} onChange={this.OnChangeName.bind(this)} className="form-control" placeholder="Entrez le nom de l'évènement..." />
          </div>
          <div className="form-group">
            <label>Lieu de l'évènement</label>
            <input value={this.state.lieu} onChange={this.OnChangeLieu.bind(this)} className="form-control" placeholder="Entrez le lieu de l'évènement..." />
          </div>
          <div>
          <p>Date de l'évènement </p>
          <DatePicker 
              selected={this.state.dateevent}
              onChange={this.onChangeDate}
              dateFormat="dd-MM-yyyy"
              locale='fr'
              minDate={this.state.dateevent}
              />
            <br />
            <br />
            <p>Heure de l'évènement </p>
            <TimePicker
              onChange={this.onChangeTime}
              value={this.state.timeevent}
            />
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
            <Link to="/">
                <button style={{marginLeft: "15px"}} type="submit" className="btn btn-success">Voir la liste des évènements</button>
            </Link>
        </form>
    </div>
        )
    }
}

