import React, { Component } from 'react'
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
import fr from 'date-fns/locale/fr';
import { registerLocale } from  "react-datepicker";
import TimePicker from 'react-time-picker'
import { Link } from 'react-router-dom'
registerLocale('fr', fr)


export default class EditEvent extends Component {

  //Constructeur
    constructor(props){
        super(props);
        this.state = {
        name: '',
        lieu: '',
        dateevent: '',
        timeevent: '',
        noData: false,
        events: [],
        redirect: false,
        };
    }

    //Récupération des priopriétés de l'évènement en base avec son ID
     componentDidMount() {
        fetch('http://localhost:8081/events/'+this.props.match.params.id)
          .then((response) => response.json())
          .then((result) => this.setState({ name: result.rows.nom, lieu: result.rows.lieu, dateevent: new Date(result.rows.dateevent), timeevent: result.rows.timeevent }));
      }
    
      //Handler pour changement de nom dans l'input
      OnChangeName(event) {
        this.setState({
          name: event.target.value,
        });
      }
    
      //Handler pour changement de lieu dans l'input
      OnChangeLieu(event) {
        this.setState({
          lieu: event.target.value,
        });
      }
    
      //Handler pour changement de date dans l'input
      onChangeDate = date => this.setState({dateevent : date})
    
      //Handler pour changement de l'heure dans l'input
      onChangeTime = val => this.setState({timeevent : val})
      
      //Fonction de modification d'un évènement par son ID
      updateItem = i => {
        console.log(i)
        fetch('http://localhost:8081/events/'+i, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            id: i,
            nom: this.state.name,
            lieu: this.state.lieu,
            dateevent: this.state.dateevent,
            timeevent: this.state.timeevent 
          }),
        }).then((response) => console.log(response));
        this.setState({
          lieu: '',
        });
        this.setState({
          name: '',
        });
       alert("L'évènment à bien été modifié !")
      }

    render() {
        return (
          <div>
            <p>ID : {this.props.match.params.id}</p>
              <div>
                <h1>Modifier un évènement</h1>
                  <form onSubmit={this.handleSubmit}> 
                    <div className="form-group">
                      <label>Nom de l'évènement</label>
                        <input value={this.state.name} onChange={this.OnChangeName.bind(this)} className="form-control" placeholder="Entrez le nom de l'évènement..." />
                    </div>
                      <div className="form-group">
                        <label>Lieu de l'évènement</label>
                          <input value={this.state.lieu} onChange={this.OnChangeLieu.bind(this)} className="form-control" placeholder="Entrez le lieu de l'évènement..." />
                      </div>
                    <div>
                    <p>Date de l'évènement </p>
                      <DatePicker 
                      selected={this.state.dateevent}
                      onChange={this.onChangeDate}
                      dateFormat="dd-MM-yyyy"
                      locale='fr'
                      />
                      <br />
                      <br />
                      <p>Heure de l'évènement </p>
                      <TimePicker
                        onChange={this.onChangeTime}
                        value={this.state.timeevent}
                      />
                    </div>
                      <button onClick={() => this.updateItem(this.props.match.params.id)} className="btn btn-primary">Modifier</button>
                      <Link to="/">
                          <button style={{marginLeft: "15px"}} type="submit" className="btn btn-success">Voir la liste des évènements</button>
                      </Link>
                  </form>
              </div>
          </div>
        )
    }
}

