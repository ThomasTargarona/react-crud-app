import React, { Component } from 'react'
import EventItem from './EventItem';
import { Link } from 'react-router-dom'

class Events extends Component {

    render() {
        //Affichage des évènements contenus dans le tableau events issu des props
        const eventsItem = this.props.events.map((event, i) => (
            <ul className="list-group" key={i}>
                <p><b>Evenement numéro {i+1}</b></p>
                <EventItem event={event}/>
            </ul>   
        ))
        return (
            <div>
                {eventsItem}
                <Link to="/addEvent">
                    <button type="submit" className="btn btn-primary">Ajouter un évènement</button>
                </Link>
            </div>
        )
    }
}

export default Events;