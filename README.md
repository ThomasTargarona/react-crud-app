# Gestion d'évènements horodatés

Cette application est une interface utilisateur permettant de manipuler des évènements horadatés (CRUD). Elle se compose de 3 parties qui communiquent entre-elles :

  - L'application frontend (IHM) est écrite en ReactJS.
  - Le backend (API) est réalisé à l'aide de NodeJS et utilise le framework ExpressJS pour le routage et l'accès aux données. Cette API respecte les standards et principes REST.
  - Une base de données SQLite.

# Fonctionnalités 

  - L'utilisateur peut ajouter un évènement en remplissant les champs au travers d'un formulaire (il ne peut par contre par ajouter un évènement dans le passé).
  - L'utilisateur peut voir la liste des évènements déjà renseignés.
  - L'utilisateur peut modifier un évènement.
  - L'utilisateur peut supprimer un évènement.

### Installation avec Docker-compose
L'application nécessite Docker et Docker Compose pour être lancée correctement.
Dans chaque dossier (frontend et backend) se trouve un Dockerfile contenant les instructions pour que le container soit monté.
A la racine du projet se trouve un fichier docker-compose.yml. C'est lui qui va orchestrer les différents containers du projet et les lancer ensemble.

Lancement du projet
```sh
$ cd react-crud-app
$ sudo docker-compose build #construction des containers
$ sudo docker-compose up -d #lancement des services
```
L'option -d permet aux containers de tourner en arrière plan.

Vérification des containers

```sh
$ sudo docker ps
```
Cette commande permet de vérifier que nos 2 containers sont effectivement lancés.

Vous pouvez maintenant ouvrir votre navigateur à l'adresse : http://localhost:3001.
Le container backend est accessible sur le port 8081 de votre machine, tandis que le container frontend est accessible sur le port 3001.

Stopper les containers

```sh
$ sudo docker-compose down
```

### Installation avec Docker
Vous pouvez utiliser Docker pour construire les images séparément et les lancer dès le build terminé.

Build du container backend
```sh
$ cd backend
$ sudo docker build -t <your-username-docker>/<your-image-name> .
```

Lancement du container backend
```sh
$ cd backend
$ sudo docker run -p <nonused_port>:8080 -d <your username>/<your-image-name>
```

Build du container frontend
```sh
$ cd frontend
$ sudo docker build -t <your-username-docker>/<your-image-name> .
```
Lancement du container frontend
```sh
$ cd frontend
$ sudo docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 3001:3000 -e CHOKIDAR_USEPOLLING=true <your-username-docker>/<your-image-name>
```



