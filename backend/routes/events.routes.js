//Connexion à la base SQLite
var db = require('../connection')
//Import des dépendences nécessaires
const express = require("express");

//Objet de routage
const apiRoutes = express.Router()

//Liste des évènements
apiRoutes.get("/", (req, res, next) => {
    var sql = "SELECT * FROM events"
    var params = []
    db.all(sql, params, (err, rows) => {
        console.log(params)
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            rows
        })
      });
});

//Recupère un évènement par son id
apiRoutes.get("/:id", (req, res, next) => {
    var sql = "SELECT * FROM events WHERE id = ?"
    var params = [req.params.id]
    db.get(sql, params, (err, rows) => {
        console.log(rows)
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        if(!rows){
           res.json({ message: 'Error', value: 'ID does not exists' });
        } else {
            res.json({
              rows
            })
        }
      });
});

//Ajout d'un évènement
apiRoutes.post("/", (req, res, next) => {
    var errors=[]
    var data = {
        nom: req.body.nom,
        lieu: req.body.lieu,
        dateevent : req.body.dateevent,
	timeevent : req.body.timeevent,
    }
    var sql ='INSERT INTO events (nom, lieu, dateevent, timeevent) VALUES (?,?,?,?)'
    var params =[data.nom, data.lieu, data.dateevent, data.timeevent]
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})

//Modification d'un évènement déjà présent en base
apiRoutes.put("/:id", (req, res, next) => {
    var sql = "UPDATE events SET nom = ?, lieu = ?, dateevent = ?, timeevent = ? WHERE id = ?"
    var params = [req.body.nom,req.body.lieu,req.body.dateevent,req.body.timeevent,req.params.id]
    db.run(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            rows
        })
      });
});

//Suppression d'un évènement
apiRoutes.delete("/:id", (req, res, next) => {
    var sql = "DELETE FROM events WHERE id = ?"
    var params = [req.params.id]
    db.run(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            message: 'Row successfully deleted'
        })
      });
});

module.exports = apiRoutes