const path = require("path");
const sqlite3 = require("sqlite3").verbose();

const DBSOURCE = "events.db"
const dbpath = path.join(__dirname, "data", DBSOURCE)

let db = new sqlite3.Database(dbpath, (err) => {
    if (err) {
      // Erreur ouverture base
      console.error(err.message)
      throw err
    }else{
        console.log('Connected to the SQLite database.')
        //Création de la table
        db.run(`CREATE TABLE events (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            nom text, 
            lieu text, 
            dateevent text, 
            timeevent text
            )`,
        (err) => {
            if (err) {
                // Table already created
                //console.log(err)
            }else{
                 // Créations de quelques éléments en base
                var insert = 'INSERT INTO events (nom, lieu, dateevent, timeevent) VALUES (?,?,?,?)'
                db.run(insert, ["Marathon","Bordeaux","10/10/2020","08:00"]) 
            }
        });  
    }
});

module.exports = db;
