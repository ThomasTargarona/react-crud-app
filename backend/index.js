const express = require("express");
const cors = require('cors');
const bodyParser = require('body-parser')

const app = express();
const port = 8080
const HOST = '0.0.0.0';

app.use(bodyParser.json())

//cors
app.use(cors())
app.options('*',cors())

//Routage évènements
const events = require('./routes/events.routes')
app.use('/events', events)

app.listen(port, () => {
  console.log("Serveur démarré (http://localhost:8080/) !");
});


